Feature: N4R-01 LogIn

    In order to have security in the API 
    As a user of the nimbus 4 rest API 
    Must be able to Log in and obtain a security Token of success if i use valid data other way get an error status code
    
    Scenario Outline: 01 LogIn with valid and not valid data
     
        When Log in with the following parameters: username = "<username>" and password = "<password>"
        Then The page most be "<page>"
        And  The message most be "<message>"

    Examples:

        |username                  			|password   |page 		|message|
        |distributor@verecloud.com 			|r1tew0rk   |login      |e_msg_1|

        