package com.verecloud.nimbus4.test.portal.common

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import com.google.common.base.Function;
import com.google.common.base.Predicate;

/**
 * 
 * @author abraham.gamaliel.morales.cortes
 * @version 1.0
 *
 */
public abstract class GenericPage {
	
	public static final int MAX_TIME_WATING = 45;
	public static final int POOLING_TIME    = 650;
	
	protected WebDriver     driver;
	protected Browser       browser;
	
	public void setBrowser(Browser browser) {
		this.browser = browser;
		driver = browser.getDriver();
	}
}