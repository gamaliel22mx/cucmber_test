package com.verecloud.nimbus4.test.portal.steps

import static cucumber.api.groovy.EN.And
import static cucumber.api.groovy.EN.Given
import static cucumber.api.groovy.EN.Then
import static cucumber.api.groovy.EN.When

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.testng.Assert

import com.verecloud.nimbus4.test.portal.common.Browser

/**
 *
 * @author Abraham Gamaliel Morales Cortes
 * @version 1.0
 *
 * */

	
	
    When(~'^Log in with the following parameters: username = "(.*?)" and password = "(.*?)"$') { String username, String password ->
		println "when .................................................."
		
		Browser browser = Browser.getInstance(Browser.PHANTOM_JS_BROWSER);
		
		//Browser browser = Browser.getInstance(Browser.CHROME_BROWSER);
		
		browser.loadUrl "https://ciitg-bluesky.verecloud.com"
		
		WebDriver driver = browser.getDriver();
		
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		WebElement input_email = driver.findElement(By.id("email"));
		
		WebElement input_password = driver.findElement(By.id("password"));
		
		WebElement button_LogIn = driver.findElement(By.xpath("//*[@id=\"login_popup\"]/div/div/div[2]/form/button"));
		
		Assert.assertNotNull(input_email);
		
		Assert.assertNotNull(input_password);
		
		Assert.assertNotNull(button_LogIn);
		
		input_email.sendKeys(username);
		
		input_password.sendKeys(password);
		
		button_LogIn.click();
		
		try {
			Thread.sleep(5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		browser.close()
		
		println "when .................................................."
    }

    Then(~'^The page most be "(.*?)"$') { String statusCode ->
		 println "Then .................................................."
    }
	
	And(~'^The message most be "(.*?)"$') { String message ->
		println "And ....................................................."
    }