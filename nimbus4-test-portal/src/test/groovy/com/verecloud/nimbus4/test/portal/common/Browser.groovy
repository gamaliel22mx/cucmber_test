package com.verecloud.nimbus4.test.portal.common

import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.phantomjs.PhantomJSDriverService
import org.openqa.selenium.remote.DesiredCapabilities

/**
 * 
 * @author abraham.gamaliel.morales.cortes
 * @version 1.0
 *
 */
public final class Browser {

	public static final int    PHANTOM_JS_BROWSER   = 0;
	public static final int    CHROME_BROWSER      = 1;

	protected static WebDriver driver;

	private Browser(int browser) {
		switch (browser) {
			case PHANTOM_JS_BROWSER:
				initPhantomJsDriver();
				break;
			case CHROME_BROWSER:
				initChromDriver();
				break;
		}
	}

	private void initPhantomJsDriver() {
		try {
			String pathPhantomjs = System.getProperty("pathDriver");
			DesiredCapabilities capability = DesiredCapabilities.phantomjs();
			capability.setCapability("takesScreenshot", true);
			capability.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, pathPhantomjs);
			driver = new PhantomJSDriver(capability);
		} catch (Exception e) {
			System.out.println("----------------------------------------------------------------");
			System.out.println("Error driver PhantomJs " + e.getMessage());
			e.printStackTrace();
			System.out.println("----------------------------------------------------------------");
		}
	}

	private void initChromDriver() {
		try {
			System.setProperty("webdriver.chrome.driver", "C:\\temp\\chromedriver.exe");
			driver = new ChromeDriver();
		} catch (Exception e) {
			System.out.println("----------------------------------------------------------------");
			System.out.println("Error driver firefox " + e.getMessage());
			e.printStackTrace();
			System.out.println("----------------------------------------------------------------");
		}
	}

	public static Browser getInstance(int browser) {
		return new Browser(browser);
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void loadUrl(String url) {
		driver.get(url);
	}
	
	public void close() {
		try {
			driver.quit();
			driver.close();
		} catch (Exception e) {
		}
	}
}