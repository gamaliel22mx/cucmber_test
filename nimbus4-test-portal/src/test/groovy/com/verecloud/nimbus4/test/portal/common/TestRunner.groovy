package com.verecloud.nimbus4.test.portal.common

import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

import cucumber.api.CucumberOptions
import cucumber.api.testng.CucumberFeatureWrapper
import cucumber.api.testng.TestNGCucumberRunner

/**
 *
 * @author Abraham Gamaliel Morales Cortes
 * @version 1.0
 *
 * */
@CucumberOptions(
format = [ 'pretty',
	'html:target/test-report',
	'json:target/test-report.json',
	'junit:target/test-report.xml' ],
features = 'src/test/resources/features',
glue = 'src/test/groovy/com/verecloud/nimbus4/test/portal/steps')
public class TestRunner{

	private TestNGCucumberRunner testNGCucumberRunner;

	@BeforeClass(alwaysRun = true)
	public void setUpClass() throws Exception {
		testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
	}

	@Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
	public void feature(CucumberFeatureWrapper cucumberFeature) {
		testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
	}

	@DataProvider
	public Object[][] features() {
		return testNGCucumberRunner.provideFeatures();
	}

	@AfterClass
	public void tearDownClass() throws Exception {
		testNGCucumberRunner.finish();
	}
}